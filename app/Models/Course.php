<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'url',
        'description',
        'coordinator',
        'mec_standard',
        'institution_id'
    ];

    /**
     * Classes relationship
     *
     * @return void
     */
    public function classes()
    {
        return $this->hasMany(Classes::class);
    }

    /**
     * Get course by request url
     *
     * @param string $url
     * @return void
     */
    public function getCourse($url)
    {
        return $this->where('url', '=', $url)->first();
    }
}
