<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'password',
        'image',
        'cpf',
        'rg',
        'birthdate',
        'phone',
        'address',
        'number',
        'neighborhood',
        'state',
        'postal_code',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Classes relationship
     *
     * @return void
     */
    public function classes()
    {
        return $this->belongsToMany(Classes::class);
    }

    /**
     * Get student by request id
     *
     * @param string $url
     * @return void
     */
    public function getStudent($id)
    {
        return $this->where('id', '=', $id)->first();
    }
}
