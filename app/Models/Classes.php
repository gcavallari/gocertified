<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'url',
        'start_date',
        'closing_date',
        'course_id',
    ];

    /**
     * Course relationship
     *
     * @return void
     */
    public function course()
    {
    	return $this->belongsTo(Course::class);
    }

    /**
     * Student relationship
     *
     * @return void
     */
    public function students()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Get class by request url
     *
     * @param string $url
     * @return void
     */
    public function getClasses($url)
    {
        return $this->where('url', '=', $url)->first();
    }
}
