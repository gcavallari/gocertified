<?php

/**
 * format date standard to friendly format
 *
 * @param string $value
 * @param string $format
 * @return void
 */
function formatDateAndTime($value, $format = 'd/m/Y')
{
    return Carbon\Carbon::parse($value)->format($format);
}

/**
 * Remove accents to make url
 *
 * @param string $string
 * @param boolean $slug
 * @return string
 */
function removeAccents($string, $slug = false)
{
    $string = utf8_decode(mb_strtolower($string));

    $ascii['a'] = range(224, 230);
    $ascii['e'] = range(232, 235);
    $ascii['i'] = range(236, 239);
    $ascii['o'] = array_merge(range(242, 246), array(240, 248));
    $ascii['u'] = range(249, 252);
    $ascii['b'] = array(223);
    $ascii['c'] = array(231);
    $ascii['d'] = array(208);
    $ascii['n'] = array(241);
    $ascii['y'] = array(253, 255);

    foreach ($ascii as $key => $item) {
        $acentos = '';
        foreach ($item as $codigo) $acentos .= chr($codigo);
        $troca[$key] = '/[' . $acentos . ']/i';
    }

    $string = preg_replace(array_values($troca), array_keys($troca), $string);

    if ($slug) {
        $string = preg_replace('/[^a-z0-9]/i', $slug, $string);
        $string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
        $string = trim($string, $slug);
    }

    return $string;
}

/**
 * Get the name initials
 *
 * @param string $firstnam
 * @param string $lastname
 * @return string
 */
function getInitials($firstname, $lastname)
{
    $firstname = substr($firstname, 0, 1);
    $lastname  = substr($lastname, 0, 1);
    $initials  = $firstname . $lastname;

    return $initials;
}
