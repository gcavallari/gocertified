<?php

namespace App\Http\Controllers\Institution;

use App\Models\User;
use App\Models\Course;
use App\Models\Classes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    public function __construct(User $student, Classes $classes, Course $course)
    {
        $this->middleware('auth');
        $this->course = $course;
        $this->classes = $classes;
        $this->student = $student;
    }

    /**
     * Show the form to insert a new student
     *
     * @return view
     */
    public function create($url, $class)
    {
        $course = $this->course->getCourse($url);
        $class = $this->classes->getClasses($class);

        return view('student.create', [
            "class" => $class,
            'course' => $course
        ]);
    }

    /**
     * Store a new student
     *
     * @param string $url
     * @param Request $request
     * @return void
     */
    public function store(Request $request, $url, $class)
    {
        $student = $this->student->fill($request->all());
        $student->save();

        $classes = $this->classes->getClasses($class);
        $classes->students()->attach($student->id);

        return redirect()
            ->action('Institution\ClassesController@show', ['url' => $url, 'class' => $class])
            ->with('status', 'Aluno adicionado com sucesso!');
    }

    /**
     * Display the specified student
     *
     * @param string $url
     * @param string $url
     * @return view
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form to edit the specified student
     *
     * @param int $id
     * @return view
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified student
     *
     * @param  Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified student
     *
     * @param int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show the form for insert a new student
     *
     * @return view
     */
    public function certify($url, $class, $student)
    {
        $course = $this->course->getCourse($url);
        $class = $this->classes->getClasses($class);
        $student = $this->student->getStudent($student);

        $params = [
            "class" => $class,
            "course" => $course,
            "student" => $student,
        ];

        return view('student.certificate', $params);

        return \PDF::loadView('student.certificate', $params)
            ->setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'images' => true])
            ->setPaper('a4', 'landscape')
            ->stream('certificate.pdf');
    }
}
