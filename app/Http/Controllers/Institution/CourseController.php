<?php

namespace App\Http\Controllers\Institution;

use App\Models\Course;
use App\Models\Classes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    public function __construct(Course $course)
    {
        $this->middleware('auth');
        $this->course = $course;
    }

    /**
     * Show course index page
     *
     * @return view
     */
    public function index()
    {
        $courses = $this->course->all();

        return view('courses.home', [
            "courses" => $courses
        ]);
    }

    /**
     * Show the form to create a new course
     *
     * @return view
     */
    public function create()
    {
        return view('courses.create');
    }

    /**
     * Store a new course
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $url = removeAccents($request->name, '-');
        $count = $this->course->where('url', '=', $url)->count();

        if($count != 0){
            $count = $count + 1;
            $url = $url .'-'. $count;
        }

        $this->course->create([
            'url' => $url,
            'name' => $request->name,
            'description' => $request->description,
            'coordinator' => $request->coordinator,
            'mec_standard' => isset($request->mec_standard) ? true : false,
    	]);

        return redirect()
            ->action('Institution\CourseController@index')
            ->with('status', 'Curso adicionado com sucesso!');
    }

    /**
     * Display the specified course
     *
     * @param int $id
     * @return view
     */
    public function show($id)
    {
        $course = $this->course->findOrFail($id);
        $classes = $course->classes;

        return view('courses.show', [
            "course" => $course,
            "classes" => $classes,
        ]);
    }

    /**
     * Show the form to edit the specified course
     *
     * @param int $id
     * @return view
     */
    public function edit($id)
    {
        $course = $this->course->findOrFail($id);

        return view('courses.edit', [
            "course" => $course
        ]);
    }

    /**
     * Update the specified course
     *
     * @param Request $request
     * @param int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $url = removeAccents($request->name, '-');
        $count = $this->course->where('url', '=', $url)->count();
        $course = $this->course->findOrFail($id);

        if($count != 0){
            $count = $count + 1;
            $url = $url .'-'. $count;
        }
        
        $course->update([
            'name' => $request->name,
            'url' => $url,
            'description' => $request->description,
            'coordinator' => $request->coordinator,
            'start_date' => $request->start_date,
            'closing_date' => $request->closing_date,
            'mec_standard' => isset($request->mec_standard) ? true : false,
    	]);

        return redirect()
            ->action('Institution\CourseController@index')
            ->with('status', 'Curso atualizado com sucesso!');
    }

    /**
     * Remove the specified course
     *
     * @param int $id
     * @return void
     */
    public function destroy($id)
    {
        $course = $this->course->findOrFail($id);
        $course->delete();

        return redirect()
            ->action('Institution\CourseController@index')
            ->with('status', 'Curso excluído com sucesso!');
    }
}
