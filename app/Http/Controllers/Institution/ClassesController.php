<?php

namespace App\Http\Controllers\Institution;

use App\Models\Course;
use App\Models\Classes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClassesController extends Controller
{
    public function __construct(Classes $classes, Course $course)
    {
        $this->middleware('auth');
        $this->course = $course;
        $this->classes = $classes;
    }

    /**
     * Show the form to create a new class
     *
     * @return view
     */
    public function create($url)
    {
        $course = $this->course->getCourse($url);

        return view('classes.create', [
            "course" => $course
        ]);
    }

    /**
     * Store a new class
     *
     * @param string $url
     * @param Request $request
     * @return void
     */
    public function store(Request $request, $url)
    {
        $course = $this->course->getCourse($url);

        $url = removeAccents($request->name, '-');
        $count = $this->classes->where('url', '=', $url)->count();

        if($count != 0){
            $count = $count + 1;
            $url = $url .'-'. $count;
        }

        $this->classes->create([
            'url' => $url,
            'name' => $request->name,
            'start_date' => $request->start_date,
            'closing_date' => $request->closing_date,
            'course_id' => $course->id
    	]);

        return redirect()
            ->action('Institution\CourseController@show', ['url' => $course->id])
            ->with('status', 'Turma adicionada com sucesso!');
    }

    /**
     * Display the specified course
     *
     * @param string $url
     * @param string $url
     * @return view
     */
    public function show($url, $class)
    {
        $course = $this->course->getCourse($url);
        $class = $this->classes->getClasses($class);
        $students = $class->students;

        return view('classes.show', [
            "class" => $class,
            "course" => $course,
            "students" => $students,
        ]);
    }

    /**
     * Show the form to edit the specified class
     *
     * @param int $id
     * @return view
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified class
     *
     * @param  Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified class
     *
     * @param int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }
}
