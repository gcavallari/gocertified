<?php

namespace App\Http\Controllers\Institution;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Institution;
use App\Models\Course;

class InstitutionController extends Controller
{
    public function __construct(Institution $institution)
    {
        $this->middleware('auth');
        $this->institution = $institution;
    }

    /**
     * Display index of institution.
     *
     * @return view
     */
    public function index(Course $course)
    {
        $courses = $course->all()->count();

        return view('institution.dashboard', [
            "courses" => $courses
        ]);
    }

    /**
     * Show reports page
     *
     * @return view
     */
    public function report()
    {
        return view('institution.report');
    }
}
