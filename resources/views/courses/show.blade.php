@extends('layouts.institution')

@section('content')

@component('courses.header')
    @slot('id') {{$course->id}} @endslot
    @slot('name') {{$course->name}} @endslot
@endcomponent

<div class="container">
    <div class="row">
        <div class="col-12">

            @if(session('status'))
            @component('institution.notification')
            @slot('message')
            {{session('status')}}
            @endslot
            @endcomponent
            @endif

            <div class="card" data-toggle="lists" data-options="{&quot;valueNames&quot;: [&quot;name&quot;]}">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="card-header-title">Turmas</h3>
                        </div>

                        <div class="col-auto">
                            <a href="{{ route('classes.create', ['url' => $course->url]) }}" class="btn btn-sm btn-primary gc-btn-new">
                                <i class="fe fe-plus"></i> Nova turma
                            </a>
                        </div>
                    </div>
                </div>

                <div class="table-responsive mb-0">
                    <table class="table table-sm table-nowrap card-table">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th class="text-center">Data de cadastro</th>
                                <th class="text-center">Ações</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($classes as $class)
                            <tr>
                                <td class="align-middle">{{ $class->name }}</td>
                                <td class="align-middle text-center">{{ $class->created_at->format('d/m/Y') }}</td>
                                <td class="align-middle text-center">
                                    <a href="{{ route('classes.show', ['url' => $course->url, 'class' => $class->url]) }}" class="btn btn-sm btn-white gc-btn-new d-none d-md-inline-block">
                                        Acessar
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection