@extends('layouts.institution')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10 col-xl-8">
            <div class="header mt-md-5">
                <div class="header-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="header-pretitle">Novo Curso</h6>
                            <h1 class="header-title">Adicionar novo curso</h1>
                        </div>
                    </div>
                </div>
            </div>

            <form class="mb-4" action="{{ route('course.store') }}" method="post">
                @csrf

                <div class="form-group">
                    <label>Nome do curso</label>
                    <input type="text" class="form-control" name="name">
                </div>

                <div class="form-group">
                    <label>Nome do coordernador</label>
                    <input type="text" class="form-control" name="coordinator">
                </div>

                <div class="form-group">
                    <label class="mb-1">Descrição do curso</label>
                    <small class="form-text text-muted">Escreve uma breve descrição sobre o curso:</small>
                    <textarea class="form-control" name="description"></textarea>
                </div>

                <hr class="mt-5 mb-5">

                <div class="row align-items-center">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label class="mb-1">Padrão MEC</label>
                            <small class="form-text text-muted">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            </small>
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" name="mec_standard" id="mecInput">
                                <label class="custom-control-label gc-pointer" for="mecInput"></label>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="card bg-light border">
                            <div class="card-body">
                                <h4 class="mb-2"><i class="fe fe-alert-triangle"></i> Aviso</h4>
                                <p class="small text-muted mb-0">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="mb-5">

                <button class="btn btn-block btn-primary">
                    Adicionar curso
                </button>
                <a href="{{ route('course.index') }}" class="btn btn-block btn-link text-muted">
                    Voltar para a página de cursos
                </a>
            </form>

        </div>
    </div>
</div>

@endsection