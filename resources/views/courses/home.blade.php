@extends('layouts.institution')

@section('content')

@component('institution.header')
@endcomponent

<div class="container">
    <div class="row">
        <div class="col-12">

            @if(session('status'))
                @component('institution.notification')
                    @slot('message')
                        {{session('status')}}
                    @endslot
                @endcomponent
            @endif

            <div class="card" data-toggle="lists" data-options="{&quot;valueNames&quot;: [&quot;name&quot;]}">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="card-header-title">Cursos</h3>
                        </div>

                        <div class="col-auto">
                            <a href="{{route('course.create')}}" class="btn btn-sm btn-primary gc-btn-new">
                                <i class="fe fe-plus"></i> Novo curso
                            </a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <ul class="list-group list-group-lg list-group-flush list my-n4">
                        @foreach($courses as $course)
                        <li class="list-group-item px-0">
                            <div class="row align-items-center">
                                <div class="col-auto">
                                    <a href="#!" class="avatar avatar-lg">
                                        <span class="avatar-title rounded bg-white text-secondary">
                                            <span class="fe fe-book"></span>
                                        </span>
                                    </a>
                                </div>

                                <div class="col ml-n2">
                                    <h4 class="card-title mb-1 name"><a href="#!">{{ $course->name }}</a></h4>
                                    <p class="card-text small text-muted mb-1">
                                        Coordenador: {{ $course->coordinator }}
                                    </p>
                                    <p class="card-text small text-muted">
                                        Adicionado em {{ $course->created_at->format('d/m/Y') }}
                                    </p>
                                </div>

                                <div class="col-auto">
                                    <a href="{{ route('course.show', ['id' => $course->id]) }}" class="btn btn-sm btn-white gc-btn-new d-none d-md-inline-block">
                                        Acessar
                                    </a>
                                </div>

                                <div class="col-auto">
                                    <div class="dropdown">
                                        <a href="#" class="dropdown-ellipses dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fe fe-more-vertical"></i>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="{{ route('course.edit', ['id' => $course->id]) }}" class="dropdown-item">
                                                <i class="fe fe-edit gc-mr-5"></i> Editar
                                            </a>
                                            <hr class="my-2">
                                            <form action="{{ route('course.destroy', ['id' => $course->id]) }}" method="post">
                                                @csrf
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-link dropdown-item">
                                                    <i class="fe fe-trash-2 gc-mr-5"></i> Excluir
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>

            </div>
        </div>
    </div>

</div>

@endsection