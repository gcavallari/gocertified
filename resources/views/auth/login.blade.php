@extends('layouts.app')

@section('content')

<h1 class="display-4 text-center mb-3">Login</h1>
<p class="text-muted text-center mb-5">Acesse sua conta preenchendo os campos abaixo:</p>

<form method="POST" action="{{ route('login') }}">
    @csrf

    <div class="form-group">
        <label>Email</label>
        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" autocomplete autofocus placeholder="email@email.com">
        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col">
                <label>Senha</label>
            </div>
            @if (Route::has('password.request'))
            <div class="col-auto">
                <a href="" class="form-text small text-muted">Esqueceu sua senha?</a>
            </div>
            @endif
        </div>

        <div class="input-group input-group-merge">
            <input type="password" name="password" class="form-control form-control-appended  @error('password') is-invalid @enderror" autocomplete placeholder="Digite sua senha">
            <div class="input-group-append">
                <span class="input-group-text">
                    <i class="fe fe-lock"></i>
                </span>
            </div>
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <button class="btn btn-lg btn-block btn-primary mb-3">Login</button>

    <div class="text-center">
        <small class="text-muted text-center">
            Não tem uma conta ainda? <a href="sign-up.html">Faça seu cadastro</a>.
        </small>
    </div>
</form>

@endsection