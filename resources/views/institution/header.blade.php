<div class="header">
    <div class="container">
        <div class="header-body">
            <div class="row align-items-center">
                <div class="col-auto">
                    <div class="avatar avatar-lg avatar-4by3">
                        <img src="{{ asset('assets/img/files/file-1.jpg') }}" class="avatar-img rounded">
                    </div>
                </div>

                <div class="col">
                    <h6 class="header-pretitle">Institution</h6>
                    <h1 class="header-title">Universidade Paulista</h1>
                </div>
            </div>

            <div class="row align-items-center">
                <div class="col">
                    <ul class="nav nav-tabs nav-overflow header-tabs">
                        <li class="nav-item">
                            <a href="{{route('institution')}}" class="nav-link">Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('course.index')}}" class="nav-link">Cursos</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('institution.reports')}}" class="nav-link">Relatórios</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>