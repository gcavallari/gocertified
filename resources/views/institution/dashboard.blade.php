@extends('layouts.institution')

@section('content')

@component('institution.header')
@endcomponent

<div class="container">
    <div class="row">
        <div class="col-12 col-lg-6 col-xl">
            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="card-title text-uppercase text-muted mb-2">Certificados emitidos</h6>
                            <span class="h2 mb-0">0</span>
                        </div>
                        <div class="col-auto">
                            <span class="h2 fe fe-award text-muted mb-0"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-6 col-xl">
            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="card-title text-uppercase text-muted mb-2">Cursos cadastrados</h6>
                            <span class="h2 mb-0">{{ $courses }}</span>
                        </div>
                        <div class="col-auto">
                            <span class="h2 fe fe-book text-muted mb-0"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-6 col-xl">
            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="card-title text-uppercase text-muted mb-2">Alunos cadastrados</h6>
                            <span class="h2 mb-0">0</span>
                        </div>
                        <div class="col-auto">
                            <span class="h2 fe fe-dollar-sign text-muted mb-0"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-6 col-xl">
            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="card-title text-uppercase text-muted mb-2">Créditos disponíveis</h6>
                            <span class="h2 mb-0">R$ 0,00</span>
                        </div>
                        <div class="col-auto">
                            <span class="h2 fe fe-dollar-sign text-muted mb-0"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection