@extends('layouts.institution')

@section('content')

@component('institution.header')
@endcomponent

<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card card-inactive">
                <div class="card-body text-center">
                    <img src="{{ asset('assets/img/illustrations/scale.svg') }}" class="img-fluid" style="max-width: 182px;">
                    <h1>Não temos relatórios :(</h1>
                    <p class="text-muted">
                        Solicite relatórios para nossa equipe para ter mais informações sobre sua conta.
                    </p>
                    <a href="#!" class="btn btn-primary">Solicitar relatório</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection