<html>

<head>
    <title>{{ $course->name }} Certicate</title>
    <style>
        p{
            margin: 0;
        }

        .cert-frame {
            padding: 110px 0px;
            text-align: center;
            align-items: center;
            font-family: sans-serif;
            background-size: cover;
            background-position: center 0;
            background-image: url({{ asset('assets/img/certificate-background.png') }});
        }

        .pre-title {
            margin: 0px !important;
            color: #999;
            font-weight: 400;
        }

        .course-title {
            margin: 0px !important;
            margin-top: 5px !important;
            text-transform: uppercase;
        }

        .cert-student{
            margin: 100px 0;
        }

        .cert-presented{
            margin-bottom: 0;
            font-weight: 400;
        }

        .cert-presented-to{
            font-family: Brush Script MT, Brush Script Std, cursive;
            font-size: 50px;
            margin: 0;
        }

        .cert-footer {
            display: inline-flex;
            align-items: center;
            width: 100%;
            margin-top: 120px;
        }

        .cert-date {
            width: 30%;
            float: left;
            padding-left: 230px;
        }

        .cert-logo {
            width: 40%;
            float: left;
        }

        .cert-logo img{
            width: 180px;
        }

        .cert-signature {
            width: 30%;
            float: left;
            padding-right: 245px;
        }

        .cert-str-date, .cert-str-signature{
            font-weight: bold;
            font-size: 20px;
            padding-bottom: 8px;
        }

        .cert-line-date, .cert-line-signature{
            border-top: 1px solid #ccc;
            padding-top: 10px; 
        }
    </style>
</head>

<body>
    <div class="cert-frame">

        <div class="cert-title">
            <h2 class="pre-title">CERTIFICADO DE</h2>
            <h1 class="course-title">{{ $course->name }}</h1>
        </div>

        <div class="cert-student">
            <h3 class="cert-presented">Apresentado para</h3>
            <h2 class="cert-presented-to">{{ $student->firstname }} {{ $student->lastname }}</h2>
        </div>

        <div class="cert-course">
            <p class="course-details">
                Certificamos que
                {{ $student->firstname }} {{ $student->lastname }}
                concluiu o curso de
                <span class="course-name">{{ $course->name }}</span>
                no período de
                {{ formatDateAndTime($class->start_date) }} a {{ formatDateAndTime($class->closing_date) }}.
            </p>
        </div>

        <div class="cert-footer">
            <div class="cert-date">
                <p class="cert-str-date">{{ date('d/m/Y') }}</p>
                <p class="cert-line-date">Emissão do Certificado</p>
            </div>

            <div class="cert-logo">
                <img src="{{ asset('assets/img/logo-gc.png') }}" />
            </div>

            <div class="cert-signature">
                <p class="cert-str-signature">{{ $course->coordinator }}</p>
                <p class="cert-line-signature">Assinatura do Coordenador</p>
            </div>
        </div>

    </div>
</body>