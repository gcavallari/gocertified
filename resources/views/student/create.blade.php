@extends('layouts.institution')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10 col-xl-8">
            <div class="header mt-md-5">
                <div class="header-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="header-pretitle">Novo aluno</h6>
                            <h1 class="header-title">Adicionar novo aluno</h1>
                        </div>
                    </div>
                </div>
            </div>

            <form class="mb-4" action="{{ route('student.store', ['url' => $course->url, 'class' => $class->url]) }}" method="post">
                @csrf

                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label>Nome</label>
                            <input type="text" class="form-control" name="firstname">
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label>Sobrenome</label>
                            <input type="text" class="form-control" name="lastname">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="text" class="form-control" name="email">
                </div>

                <hr class="mt-5 mb-5">

                <button class="btn btn-block btn-primary">
                    Adicionar aluno
                </button>
                <a href="{{ route('classes.show', ['url' => $course->url, 'class' => $class->url]) }}" class="btn btn-block btn-link text-muted">
                    Voltar para {{ $class->name }}
                </a>
            </form>

        </div>
    </div>
</div>

@endsection