<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'GoCertified') }}</title>

  <!-- styles -->
  <link rel="stylesheet" href="{{ asset('assets/fonts/feather/feather.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/libs/flatpickr/dist/flatpickr.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/libs/quill/dist/quill.core.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/libs/highlight.js/styles/vs2015.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/theme.min.css') }}" id="stylesheetLight">
  <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
</head>

<body>

  <div class="main-content">
    <nav class="navbar navbar-expand-lg navbar-light" id="topnav">
      <div class="container">

        <!-- Toggler -->
        <button class="navbar-toggler mr-auto" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Brand -->
        <a class="navbar-brand mr-auto" href="{{route('institution')}}">
          GoCertified <i class="fe fe-award"></i>
        </a>

        <!-- User -->
        <div class="navbar-user">
          <div class="dropdown mr-4 d-none d-lg-flex">
            <!-- Toggle -->
            <a href="#" class="navbar-user-link" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="icon active"><i class="fe fe-bell"></i></span>
            </a>
          </div>

          <div class="dropdown">
            <a href="#" class="avatar avatar-sm dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="avatar-title rounded-circle">IN</span>
            </a>

            <div class="dropdown-menu dropdown-menu-right">
              <a href="" class="dropdown-item"><i class="fe fe-user gc-mr-5"></i> Perfil</a>
              <a href="" class="dropdown-item"><i class="fe fe-settings gc-mr-5"></i> Configurações</a>
              <hr class="dropdown-divider">
              <a href="" class="dropdown-item"><i class="fe fe-log-out gc-mr-5"></i> Logout</a>
            </div>
          </div>
        </div>

        <div class="collapse navbar-collapse mr-auto order-lg-first" id="navbar">
          <ul class="navbar-nav mr-auto">
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle " href="#" id="topnavDocumentation" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Institution</a>
              <ul class="dropdown-menu" aria-labelledby="topnavDocumentation">
                <li><a class="dropdown-item " href="{{ route('institution') }}">Dashboard</a></li>
                <li><a class="dropdown-item " href="{{ route('course.index') }}">Cursos</a></li>
                <li><a class="dropdown-item " href="{{ route('institution.reports') }}">Relatórios</a></li>
              </ul>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link" href="">Templates</a>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle " href="#" id="topnavDocumentation" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Créditos</a>
              <ul class="dropdown-menu" aria-labelledby="topnavDocumentation">
                <li><a class="dropdown-item " href="">Meus créditos</a></li>
                <li><a class="dropdown-item " href="">Adicionar créditos</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    @yield('content')

  </div>

  <!-- scripts  -->
  <script src="{{ asset('assets/libs/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('assets/libs/@shopify/draggable/lib/es5/draggable.bundle.legacy.js') }}"></script>
  <script src="{{ asset('assets/libs/autosize/dist/autosize.min.js') }}"></script>
  <script src="{{ asset('assets/libs/chart.js/dist/Chart.min.js') }}"></script>
  <script src="{{ asset('assets/libs/dropzone/dist/min/dropzone.min.js') }}"></script>
  <script src="{{ asset('assets/libs/flatpickr/dist/flatpickr.min.js') }}"></script>
  <script src="{{ asset('assets/libs/highlightjs/highlight.pack.min.js') }}"></script>
  <script src="{{ asset('assets/libs/jquery-mask-plugin/dist/jquery.mask.min.js') }}"></script>
  <script src="{{ asset('assets/libs/list.js/dist/list.min.js') }}"></script>
  <script src="{{ asset('assets/libs/quill/dist/quill.min.js') }}"></script>
  <script src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
  <script src="{{ asset('assets/libs/chart.js/Chart.extension.min.js') }}"></script>
  <script src="{{ asset('assets/js/theme.min.js') }}"></script>

</body>

</html>