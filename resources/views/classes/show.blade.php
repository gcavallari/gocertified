@extends('layouts.institution')

@section('content')

@component('classes.header')
    @slot('name') {{$class->name}} @endslot
    @slot('course') {{$course->name}} @endslot
@endcomponent

<div class="container">
    <div class="row">
        <div class="col-12">

            @if(session('status'))
            @component('institution.notification')
            @slot('message')
            {{session('status')}}
            @endslot
            @endcomponent
            @endif

            <div class="card" data-toggle="lists" data-options="{&quot;valueNames&quot;: [&quot;name&quot;]}">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="card-header-title">Alunos</h3>
                        </div>

                        <div class="col-auto">
                            <a href="{{ route('student.create', ['url' => $course->url, 'class' => $class->url]) }}" class="btn btn-sm btn-primary gc-btn-new">
                                <i class="fe fe-plus"></i> Novo aluno
                            </a>
                        </div>
                    </div>
                </div>

                <div class="table-responsive mb-0">
                    <table class="table table-sm table-nowrap card-table">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th class="text-center">Data de cadastro</th>
                                <th class="text-center">Ações</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($students as $student)
                            <tr>
                                <td class="align-middle">
                                    <a class="avatar avatar-sm d-inline-block mr-2">
                                        <span class="avatar-title rounded-circle">
                                            {{ getInitials($student->firstname, $student->lastname) }}
                                        </span>
                                    </a>
                                    <span>{{ $student->firstname }} {{ $student->lastname }}</span>
                                </td>
                                <td class="align-middle text-center">{{ $student->created_at->format('d/m/Y') }}</td>
                                <td class="align-middle text-center">
                                    <a href="{{ route('student.certify', ['url' => $course->url, 'class' => $class->url, 'student' => $student->id]) }}" target="_blank" class="btn btn-sm btn-white gc-btn-new d-none d-md-inline-block">
                                        Emitir certificado
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection