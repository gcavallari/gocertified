@extends('layouts.institution')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10 col-xl-8">
            <div class="header mt-md-5">
                <div class="header-body">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="header-pretitle">Nova Turma</h6>
                            <h1 class="header-title">Adicionar nova turma</h1>
                        </div>
                    </div>
                </div>
            </div>

            <form class="mb-4" action="{{ route('classes.store', ['url' => $course->url]) }}" method="post">
                @csrf

                <div class="form-group">
                    <label>Nome da turma</label>
                    <input type="text" class="form-control" name="name">
                </div>

                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label>Data de início</label>
                            <input type="text" class="form-control flatpickr-input" name="start_date" data-toggle="flatpickr" readonly="readonly" placeholder="Clique para selecionar a data">
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label>Data de conclusão</label>
                            <input type="text" class="form-control flatpickr-input" name="closing_date" data-toggle="flatpickr" readonly="readonly" placeholder="Clique para selecionar a data">
                        </div>
                    </div>
                </div>

                <hr class="mb-5">

                <button class="btn btn-block btn-primary">
                    Adicionar turma
                </button>
                <a href="{{ route('course.show', ['url' => $course->id]) }}" class="btn btn-block btn-link text-muted">
                    Voltar para {{$course->name}}
                </a>
            </form>

        </div>
    </div>
</div>

@endsection