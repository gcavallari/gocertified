<div class="header">
    <div class="container">
        <div class="header-body">
            <div class="row align-items-center">
                <div class="col">
                    <h6 class="header-pretitle">{{ $course }}</h6>
                    <h1 class="header-title">{{ $name }}</h1>
                </div>
            </div>

            <div class="row align-items-center">
                <div class="col">
                    <ul class="nav nav-tabs nav-overflow header-tabs">
                        <li class="nav-item">
                            <a href="" class="nav-link">Alunos</a>
                        </li>
                        <li class="nav-item">
                            <a href="" class="nav-link">Certificados</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>