<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Auth::routes();

Route::get('/', function () { return view('welcome'); });
Route::get('/home', 'HomeController@index')->name('home');

//Institution
Route::prefix('institution')->group(function () {

    Route::get('/', 'Institution\InstitutionController@index')->name('institution');
    Route::get('/report', 'Institution\InstitutionController@report')->name('institution.reports');

    Route::resource('course', 'Institution\CourseController')->parameters(['course' => 'url']);
    
    Route::get('/{url}/{class}', 'Institution\ClassesController@show')->name('classes.show');
    Route::get('/{url}/classes/create', 'Institution\ClassesController@create')->name('classes.create');
    Route::post('/{url}/classes/create', 'Institution\ClassesController@store')->name('classes.store');

    Route::get('/{url}/{class}/student/create', 'Institution\StudentController@create')->name('student.create');
    Route::post('/{url}/{class}/student/create', 'Institution\StudentController@store')->name('student.store');
    Route::get('/{url}/{class}/{student}/certify', 'Institution\StudentController@certify')->name('student.certify');

});
